prewikka for Debian
-------------------

The Prewikka installation guide is available at:
https://www.prelude-siem.org/projects/prelude/wiki/InstallingPreludePrewikka


Editing prewikka.conf
=====================

Once you have created the database for Prewikka you need to edit
/etc/prewikka/prewikka.conf to fit your database settings prior to starting
Prewikka. The debian installation script will try to create the database and
set the values, for the [database] section ONLY. Even if you used the
installer to create the database and tables, you will need to edit the
configuration file for other values.

#The following are the setting for your prelude database
[idmef_database]
type: mysql
host: localhost
user: prelude
pass: prelude
name: prelude

#This is the database information for the prewikka DB you created above
[database]
type: mysql
host: localhost
user: prewikka
pass: prewikka
name: prewikka

If you are experimenting issues with mysql/mariadb with messages like "Index
column size too large. The maximum column size is ...", follow this:
- Drop your actual database
- Recreate your database with "COLLATE 'utf8_unicode_ci'" option

Choose one of the following methods to configure your web server:

Running Prewikka from the Apache web server (mod_wsgi)
======================================================

<VirtualHost *:80>
        WSGIApplicationGroup %{GLOBAL}
        WSGIScriptAlias / /var/www/html/prewikka.wsgi
</VirtualHost>

Content of /var/www/html/prewikka.wsgi:

from prewikka.web import wsgi
application = wsgi.application

Running Prewikka from the command line tool
===========================================

Just run:

   $ /usr/bin/prewikka-httpd

You can then use your browser to connect to your machine on port 8000.



Initial login
=============

Once everything is setup, you can use your browser to connect to the machine
were Prewikka was installed. If you are not using Apache support, then remember
you should use the port 8000 to access Prewikka.

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Tue, 26 Dec 2017 14:00:00 +0100
