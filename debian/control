Source: prewikka
Section: web
Priority: optional
Maintainer: Pierre Chifflier <pollux@debian.org>
Uploaders: Thomas Andrejak <thomas.andrejak@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
    dh-python,
    python3-all,
    python3-setuptools,
    python3-babel,
    python3-lesscpy,
    gettext,
Standards-Version: 4.6.0
Homepage: https://www.prelude-siem.org/
Vcs-Browser: https://salsa.debian.org/totol-guest/prewikka
Vcs-Git: https://salsa.debian.org/totol-guest/prewikka.git

Package: prewikka
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends},
    libjs-jquery,
    libjs-jquery-ui,
    python3-croniter (>= 0.3.6),
    python3-mako (>= 0.8.1),
    python3-prelude (>= 5.2.0),
    python3-preludedb (>= 5.2.0),
    python3-pkg-resources,
    python3-babel,
    python3-dateutil,
    python3-requests,
    python3-passlib,
    python3-tz,
    python3-werkzeug,
    python3-yaml,
    python3-gevent,
    python3-voluptuous,
    python3-lark,
    fonts-font-awesome,
    libjs-underscore,
    ucf,
    dbconfig-common,
    libpreludedb7-mysql | libpreludedb7-pgsql,
Recommends: python3-twisted
Description: Security Information and Events Management System [ Web Interface ]
 Prewikka is the graphical front-end analysis console for the
 Prelude SIEM. Providing numerous features, Prewikka
 facilitates the work of users and analysts. It provides alert
 aggregation and sensor and hearbeat views, and has configurable
 filters. Prewikka also provides access to external
 tools such as whois and traceroute.
